package fr.istic.tp3;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * Created by btessiau on 12/02/16.
 */
public class InfoRegionsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /** Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.info_regions_fragment_layout, container, false);
        WebView webView = (WebView) v.findViewById(R.id.webView);
        webView.loadUrl("www.google.fr");
        return v;
    }
}