package fr.istic.tp3;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by btessiau on 12/02/16.
 */
public class RegionsFragment extends Fragment {
    private ListView maListViewPerso;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.regions_fragment_layout, container, false);

        maListViewPerso = (ListView) v.findViewById(R.id.listviewperso);

        listItem = new ArrayList<>();
        init();

        mListAdapter = new SimpleAdapter (container.getContext(), listItem, R.layout.listviewitem,
                new String[] {"region", "lien"}, new int[] {R.id.region, R.id.lien});
        maListViewPerso.setAdapter(mListAdapter);
        maListViewPerso.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        return v;
    }

    private void init() {
        String url = "http://technoresto.org/vdf/";

        HashMap<String, String> map;

        map = new HashMap<>();
        map.put("region", "L'Alsace");
        map.put("lien", url + "alsace/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Beaujolais");
        map.put("lien", url + "beaujolais/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Jura");
        map.put("lien", url + "jura/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La Champagne");
        map.put("lien", url + "champagne/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La Savoie");
        map.put("lien", url + "savoie/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Languedoc-Roussillon");
        map.put("lien", url + "languedoc/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Bordelais");
        map.put("lien", url + "bordelais/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La vallée du Rhône");
        map.put("lien", url + "cotes_du_rhone/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La Provence");
        map.put("lien", url + "provence/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Val de Loire");
        map.put("lien", url + "val_de_loire/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "Le Sud-Ouest");
        map.put("lien", url + "sud-ouest/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La Corse");
        map.put("lien", url + "corse/index.html");
        listItem.add(map);

        map = new HashMap<>();
        map.put("region", "La Bourgogne");
        map.put("lien", url + "bourgogne/index.html");
        listItem.add(map);
    }
}