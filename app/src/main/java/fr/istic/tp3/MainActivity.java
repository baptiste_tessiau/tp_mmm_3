package fr.istic.tp3;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if (savedInstanceState != null) {
            return;
        }

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        RegionsFragment regionsFragment = new RegionsFragment();
        fragmentTransaction.add(R.id.frameLayout1, regionsFragment, "REGIONS");

        if (findViewById(R.id.frameLayout2) != null) {
            InfoRegionsFragment infoRegionsFragment = new InfoRegionsFragment();
            fragmentTransaction.add(R.id.frameLayout2, infoRegionsFragment, "INFOREGIONS");
        }
        fragmentTransaction.commit();
    }
}